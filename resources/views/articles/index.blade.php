@extends('layout')

@section('content')

  @foreach ($articles as $article)

    <div id="wrapper">
      <div id="page" class="container">
        <div id="content">
            <div class="title">
            <h2>
            <a href="{{ $article->path() }}"> {{ $article->title }} </a>
            </h2>
              <span class="byline">Mauris vulputate dolor sit amet nibh</span>
            </div>
            <p><img src="/images/banner.jpg" alt="" class="image image-full" /> </p>
            <p> {{ $article->body }} </p>
        </div>
      </div>
    </div>      
    
  @endforeach

@endsection