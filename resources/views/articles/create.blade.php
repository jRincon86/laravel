@extends('layout')

@section('head')
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
@endsection

@section('content')
  <div id="wrapper">
    <div id="page" class="container">
      <h1 class="headint has-text-weight-bold is-size-4">New Article!</h1>

      <form method="POST" action="/articles">
        @csrf

        <div class="field">
          <label for="title" class="label">Title</label>

          <div class="control">
          <input 
            type="text"
            id="title"
            name="title"
            class="input @error('title') is-danger @enderror"
            value={{ old('title') }}>

            @if ($errors->has('title'))
              <p class="help is-danger"> {{ $errors->first('title') }} </p>
            @endif
          </div>

        </div>

        <div class="field">
          <label for="excert" class="label">Excerpt</label>
          <div class="control">
            <textarea 
              name="excert"
              id="excert"
              class="textarea @error('title') is-danger @enderror"
              
            > {{ old('excert') }}</textarea>
            
            <p class="help is-danger"> {{ $errors->first('excert') }} </p>
          </div>      
        </div>

        <div class="field">
          <label for="body" class="label">Body</label>
          <div class="control">
            <textarea 
              name="body"
              id="body"
              class="textarea @error('title') is-danger @enderror"              
            > {{ old('body') }} </textarea>

            <p class="help is-danger"> {{ $errors->first('body') }} </p>
          </div>      
        </div>

        <div class="field is-grouped">
          <div class="control">
            <button class="button is-link" type="submit">Submit</button>
          </div>


        </div>
      </form>

    </div>
  </div>
@endsection