<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicacionesController extends Controller
{
    public function show($publicacion)
    {
        $publicaciones = [
            'numero1' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, natus!',
            'numero2' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Architecto pariatur esse nihil.'
        ];
    
        if(!array_key_exists($publicacion, $publicaciones)) {
            abort(404);
        }
    
        return view('publicacion', [
            'nombre' => $publicaciones[$publicacion]
        ]);
    }
}
