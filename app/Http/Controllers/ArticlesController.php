<?php

namespace App\Http\Controllers;
use App\Article;
use App\Tag;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function show(Article $article)
    {
        // $article = Article::find($id);
        return view('articles.show', ['article' => $article]);
    }

    public function index()
    // Render a list of the resource
    {
        if(request('tag')){
            $articles = Tag::where('name', request('tag'))->firstOrFail()->articles;            
        } else {
            $articles = Article::latest()->get();
        }

        return view('articles.index', ['articles' => $articles]);
    }

    public function create()
    {
        // shows a view to create a new resource
        return view('articles.create');
    }

    public function store() 
    {   
        // request()->validate([
        //     'title' => 'required',
        //     'excert' => 'required',
        //     'body' => 'required'
        // ]);

        // $article = new Article();
        // $article->title = request('title');
        // $article->excert = request('excert');
        // $article->body = request('body');

        // $article->save();

        Article::create($this->validateAttributes());

        return redirect('/articles');
    }
    public function edit(Article $article)
    {
        // Show a view to edit an existing resource
        // $article = Article::findOrFail($id);
        return view('articles.edit', compact('article'));
    }

    public function update(Article $article)
    {
        // Persist the edited resorce
        // request()->validate([
        //     'title' => 'required',
        //     'excert' => 'required',
        //     'body' => 'required'
        // ]);

        // // $article = Article::findOrFail($id);

        // $article->title = request('title');
        // $article->excert = request('excert');
        // $article->body = request('body');

        // $article->save();
        // $validatedAtrributes = request()->validate([ // request()->validate, si corre, regresa un array con los datos.
        //     'title' => 'required',
        //     'excert' => 'required',
        //     'body' => 'required'
        // ]);

        $article->update($this->validateAttributes());

        return redirect($article->path());
    }

    public function destroy(Article $article)
    {
        // Delete the resource
        // $article = Article::findOrFail($id);
       //  $article->destroy();
        return redirect('/articles');
    }

    public function validateAttributes()
    {
        $validated = request()->validate([ // request()->validate, si corre, regresa un array con los datos.
            'title' => 'required',
            'excert' => 'required',
            'body' => 'required'
        ]);
        return $validated;

    }
}
